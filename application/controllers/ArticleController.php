<?php


namespace application\controllers;


use application\core\Controller;
use application\models\ArticleModel;

class ArticleController extends Controller
{

  public function actionIndex()
  {
    $articlesObjs = ArticleModel::getAll();
    $this->view->generate('article/index.php', $articlesObjs);
  }

  public function actionCreatetable()
  {
    $modelArticle = new ArticleModel();
    $modelArticle->crateTable();

    header('location:/');
  }

  public function actionShow($id)
  {
    $data = ArticleModel::getById($id);
    $this->view->generate('article/single.php', $data);
  }

  public function actionCreate()
  {
    $this->view->generate('article/create.php');
  }

  public function actionStore()
  {

    if (isset($_REQUEST['submit']))
    {
      foreach ($_REQUEST as $key => $value)
      {
        $_REQUEST[$key] = htmlspecialchars($value);
      }

      $title = $_REQUEST['title'];
      $text = $_REQUEST['text'];

      $article = new ArticleModel($title, $text);
      $article->store();
    }
  }

  public function actionDelete($id)
  {
    $article = ArticleModel::getById($id);
    $article->delete($id);
  }

  public function actionEdit($id)
  {
    $article = ArticleModel::getById($id);
    $this->view->generate('article/edit.php', $article);
  }

  public function actionUpdate()
  {
    if (isset($_REQUEST['submit']))
    {
      foreach ($_REQUEST as $key => $value)
      {
        $_REQUEST[$key] = htmlspecialchars($value);
      }

      $id = $_REQUEST['id'];
      $title = $_REQUEST['title'];
      $text = $_REQUEST['text'];

      $portfolio = new ArticleModel($title, $text);
      $portfolio->setId($id);
      $portfolio->update();

    }
  }

}