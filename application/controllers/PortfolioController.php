<?php


namespace application\controllers;


use application\core\Controller;
use application\models\PortfolioModel;

class PortfolioController extends Controller
{
    
    
    public function actionIndex(){
        $data = PortfolioModel::getAll();
        $this->view->generate('portfolio/index.php', $data);
    }

    public function actionCreatetable()
    {
      $portfolioModel = new PortfolioModel();
      $portfolioModel->createTablePortfolio();

      header('location:/');
    }

    public function actionShow($url)
    {
      $url = htmlspecialchars($url);

      $data = PortfolioModel::getByUrl($url);

      $this->view->generate('portfolio/single.php', $data);

    }

    public function actionDelete($id)
    {
      $id = htmlspecialchars($id);
      PortfolioModel::delete($id);
    }

    public function actionCreate()
    {
      $this->view->generate('portfolio/create.php');
    }

    public function actionStore()
    {

      if (isset($_REQUEST['submit']))
      {
        foreach ($_REQUEST as $key => $value)
        {
          $_REQUEST[$key] = htmlspecialchars($value);
        }

        $title = $_REQUEST['title'];
        $year = $_REQUEST['year'];
        $url = PortfolioModel::translit($title);
        $description = $_REQUEST['description'];

        $portfolio = new PortfolioModel($title, $year, $url, $description);
        $portfolio->store();

      }
    }

    public function actionEdit($url)
    {
      $portfolio = PortfolioModel::getByUrl($url);
      $this->view->generate('portfolio/edit.php', $portfolio);
    }

    public function actionUpdate()
    {
      if (isset($_REQUEST['submit']))
      {
        foreach ($_REQUEST as $key => $value)
        {
          $_REQUEST[$key] = htmlspecialchars($value);
        }

        $id = $_REQUEST['id'];
        $title = $_REQUEST['title'];
        $year = $_REQUEST['year'];
        $url = $_REQUEST['url'];
        $description = $_REQUEST['description'];

        $portfolio = new PortfolioModel($title, $year, $url, $description);
        $portfolio->setId($id);
        $portfolio->update();

      }

    }



}