<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Cоздать портфолио</title>
</head>
<body>

<form action="/portfolio/store" method="post">
  <div><label for="">Заголовок<input type="text" name="title"></label></div>
  <div><label for="">Год<input type="text" name="year"></label></div>
  <div><label for="">Описание<input type="text" name="description"></label></div>
  <input type="submit" name="submit" value="Создать">
</form>

</body>
</html>
