<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Редактировать портфолио</title>
</head>
<body>

<form action="/portfolio/update" method="post">
  <div><label for=""><input type="hidden" name="id" value="<?=$data->getId()?>"></label></div>
  <div><label for="">Заголовок<input type="text" name="title" value="<?=$data->getTitle()?>"></label></div>
  <div><label for="">Год<input type="text" name="year" value="<?=$data->getYear()?>"></label></div>
  <div><label for="">Описание<input type="text" name="description" value="<?=$data->getDescription()?>"></label></div>
  <div><label for="">URL<input type="text" name="url" value="<?=$data->getUrl()?>"></label></div>
  <input type="submit" name="submit" value="Редактировать">
</form>



</body>
</html>
