<?php

namespace application\traites;

trait DbConnection
{
  protected static $db = null;

  protected static function setDb()
  {
    try
    {
      $dsn = 'mysql:host='.$_ENV['DB_HOST'].';dbname='.$_ENV['DB_NAME'];
      $pdo = new \PDO($dsn, $_ENV['DB_USER'], $_ENV['DB_PASS']);
      $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
      $pdo->exec('SET NAMES utf8');

      return $pdo;
    }
    catch (Exception $e)
    {
      echo 'Error to connect to db! Code: '.$e->getCode().'. Message: '.$e->getMessage();    }

  }

  protected static function getDb()
  {

    if (!self::$db)
    {
      self::$db = self::setDb();
    }

    return self::$db;
  }

}