<?php
namespace application\models;

use application\core\Model;
use application\traites\DbConnection;

class PortfolioModel extends Model
{

  protected $id;
  protected $title;
  protected $year;
  protected $url;
  protected $description;

  public function __construct($title, $year, $url, $description)
  {
    $this->title = $title;
    $this->year = $year;
    $this->url = $url;
    $this->description = $description;
  }

  public function getId()
  {
    return $this->id;
  }

  public function setId($id)
  {
    $this->id = $id;
  }

  public function getTitle()
  {
    return $this->title;
  }

  public function setTitle($title)
  {
    $this->title = $title;
  }

  public function getYear()
  {
    return $this->year;
  }

  public function setYear($year)
  {
    $this->year = $year;
  }

  public function getUrl()
  {
    return $this->url;
  }

  public function setUrl($url)
  {
    $this->url = $url;
  }

  public function getDescription()
  {
    return $this->description;
  }

  public function setDescription($description)
  {
    $this->description = $description;
  }

  public function createTablePortfolio()
  {
    try
    {
      $pdo = self::getDb();
      $sql = 'CREATE TABLE portfolio
            (
              id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
              title VARCHAR (255),
              year VARCHAR (255),
              url VARCHAR (255),
              description VARCHAR (255)
            )
            DEFAULT CHARACTER SET utf8 ENGINE=InnoDB
            ';
      $pdo->exec($sql);
    }
    catch (\Exception $e)
    {
      echo 'Error, while creating table portfolio! Code: '.$e->getCode().'. Message: '.$e->getMessage();
    }

  }

  public static function getAll()
  {
    try
    {
      $pdo = self::getDb();
      $sql = 'SELECT * FROM portfolio';
      $sth = $pdo->query($sql);
      $portfolioArr = $sth->fetchAll();

      $portfolioObjs = [];

      foreach ($portfolioArr as $portfolio)
      {
        $portfolioObj = new self($portfolio['title'], $portfolio['year'], $portfolio['url'], $portfolio['description']);
        $portfolioObj->setId($portfolio['id']);

        $portfolioObjs[] = $portfolioObj;
      }

      return $portfolioObjs;
    }
    catch (\Exception $e)
    {
      echo 'Error, while getting data from portfolio table! Code: '.$e->getCode().'. Message: '.$e->getMessage();
    }

  }

  public static function getByUrl($url)
  {
    try
    {
      $pdo = self::getDb();
      $sql = 'SELECT * FROM portfolio 
              WHERE url = :url';
      $sth = $pdo->prepare($sql);
      $sth->bindValue(':url', $url);
      $sth->execute();

      $portfolioArr = $sth->fetchAll();

      $portfolioObj = new self($portfolioArr[0]['title'], $portfolioArr[0]['year'], $portfolioArr[0]['url'], $portfolioArr[0]['description']);
      $portfolioObj->setId($portfolioArr[0]['id']);

      return $portfolioObj;
    }
    catch (\Exception $e)
    {
      echo 'Error, while getting data from portfolio table! Code: '.$e->getCode().'. Message: '.$e->getMessage();
    }
  }

  public static function delete($id)
  {
    $portfolioObj = self::getByUrl($id);
    $portfolioObj->destroy();
  }

  public function destroy()
  {
    $pdo = self::getDb();
    $sql = 'DELETE FROM portfolio 
            WHERE id = :id
           ';
    $sth = $pdo->prepare($sql);
    $sth->bindValue(':id', $this->id);
    $sth->execute();
    header('location:/portfolio/');
  }

  public function store()
  {
    $pdo = self::getDb();
    $sql = 'INSERT INTO portfolio SET
           title = :title,
           year = :year,
           url = :url,
           description = :description 
          ';
    $sth = $pdo->prepare($sql);
    $sth->bindValue(':title', $this->title);
    $sth->bindValue(':year', $this->year);
    $sth->bindValue(':url', $this->url);
    $sth->bindValue(':description', $this->description);
    $sth->execute();

    header('location:/portfolio/');
  }

  public function update()
  {
    $pdo = self::getDb();
    $sql = 'UPDATE portfolio SET
           title = :title,
           year = :year,
           url = :url,
           description = :description 
           WHERE id = :id 
          ';
    $sth = $pdo->prepare($sql);
    $sth->bindValue(':id', $this->id);
    $sth->bindValue(':title', $this->title);
    $sth->bindValue(':year', $this->year);
    $sth->bindValue(':url', $this->url);
    $sth->bindValue(':description', $this->description);
    $sth->execute();

    header('location:/portfolio/show/'.$this->url);

  }

  public static function translit($string) {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '',    'ы' => 'y',   'ъ' => '',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '',  'Ы' => 'Y',   'Ъ' => '',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    );
    $string = strtr($string, $converter);
    $string = mb_strtolower($string);
    $string = preg_replace('/[ ]/u', '-', $string);
    $string = preg_replace('/[^\w-]/u', '', $string);
    $result = trim($string, '-');

    return $result;
  }


}