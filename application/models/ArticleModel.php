<?php


namespace application\models;


use application\core\Model;

class ArticleModel extends Model
{
  protected $id;
  protected $title;
  protected $text;

  public function __construct($title, $text)
  {
    $this->title = $title;
    $this->text = $text;
  }

  public function getId()
  {
    return $this->id;
  }

  public function setId($id)
  {
    $this->id = $id;
  }

  public function getTitle()
  {
    return $this->title;
  }

  public function setTitle($title)
  {
    $this->title = $title;
  }


  public function getText()
  {
    return $this->text;
  }


  public function setText($text)
  {
    $this->text = $text;
  }


  public function crateTable()
  {
    try
    {
      $pdo = self::getDb();
      $sql = 'CREATE TABLE article
            (
                id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                title VARCHAR (255),
                text VARCHAR (255)                            
            ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB';
      $pdo->exec($sql);
    }
    catch (\Exception $e)
    {
      echo 'Error, while creating table article! Code: '.$e->getCode().'. Message: '.$e->getMessage();
    }
  }

  public static function getAll()
  {
    try
    {
      $pdo = self::getDb();
      $sql = 'SELECT * FROM article';
      $sth = $pdo->query($sql);
      $articleArr = $sth->fetchAll();

      $articleObjs = [];

      foreach ($articleArr as $article)
      {
        $articleObj = new self($article['title'], $article['text']);
        $articleObj->setId($article['id']);

        $articleObjs[] = $articleObj;
      }

      return $articleObjs;
    }
    catch (\Exception $e)
    {
      echo 'Error, while getting data from article table! Code: '.$e->getCode().'. Message: '.$e->getMessage();
    }

  }

  public static function getById($id)
  {
    try
    {
      $pdo = self::getDb();
      $sql = 'SELECT * FROM article 
              WHERE id = :id';
      $sth = $pdo->prepare($sql);
      $sth->bindValue(':id', $id);
      $sth->execute();

      $articleArr = $sth->fetchAll();

      $articleObj = new self($articleArr[0]['title'], $articleArr[0]['text']);
      $articleObj->setId($articleArr[0]['id']);

      return $articleObj;
    }
    catch (\Exception $e)
    {
      echo 'Error, while getting data from article table! Code: '.$e->getCode().'. Message: '.$e->getMessage();
    }
  }

  public function store()
  {
    $pdo = self::getDb();
    $sql = 'INSERT INTO article SET
           title = :title,
           text = :text 
          ';
    $sth = $pdo->prepare($sql);
    $sth->bindValue(':title', $this->title);
    $sth->bindValue(':text', $this->text);
    $sth->execute();

    header('location:/article/');
  }

  public function delete($id)
  {
    $pdo = self::getDb();
    $sql = 'DELETE FROM article 
            WHERE id = :id
           ';
    $sth = $pdo->prepare($sql);
    $sth->bindValue(':id', $this->id);
    $sth->execute();
    header('location:/article/');
  }

  public function update()
  {
    $pdo = self::getDb();
    $sql = 'UPDATE article SET
           title = :title,
           text = :text  
           WHERE id = :id 
          ';
    $sth = $pdo->prepare($sql);
    $sth->bindValue(':id', $this->id);
    $sth->bindValue(':title', $this->title);
    $sth->bindValue(':text', $this->text);
    $sth->execute();

    header('location:/article/show/'.$this->id);
  }

}