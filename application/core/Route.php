<?php


namespace application\core;


class Route
{
    static public function start(){
        $controllerName = 'home';
        $actionName = 'index';

        $routes = explode('/', trim($_SERVER['REQUEST_URI'], '/'));

        if(!empty($routes[0])){
            $controllerName = $routes[0];
        }

        if(!empty($routes[1])){
            $actionName = $routes[1];
        }

        if(!empty($routes[2])){
          $parametr = $routes[2];
        }

        $controllerName = '\\application\\controllers\\'.ucfirst($controllerName).'Controller';
        $actionName = 'action'.ucfirst($actionName);
        


        $controller = new $controllerName;//new \application\controllers\PortfolioController
        $controller->$actionName($parametr); // $controller->actionIndex(); $controller->actionContacts();
        
    }
}