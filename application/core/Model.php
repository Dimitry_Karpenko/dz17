<?php

namespace application\core;

use application\traites\DbConnection;

class Model
{
  use DbConnection;
}