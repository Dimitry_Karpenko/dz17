<?php


namespace application\core;


class View
{
    protected $layout = 'layouts/basic.php';

    public function generate($view, $data = null, $layout = null){
        if($layout){
            $this->layout = $layout;
        }


        include 'application/views/'.$this->layout;
    }

}